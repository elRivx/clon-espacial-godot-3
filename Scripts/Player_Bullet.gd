extends Sprite

var speed = 100

func _physics_process(delta):
	global_position.x += speed * delta
	
	if global_position.x > 224:
		queue_free()
