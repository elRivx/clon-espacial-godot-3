extends Sprite

var bullet = preload("res://Others/Player_Bullet.tscn")
var particles = preload("res://Others/Player_destroy_particles.tscn")

signal create_bullet(bullet, location)

func _physics_process(delta):
	global_position.y = lerp(global_position.y, get_viewport().get_mouse_position().y, 0.2)
	
	if Input.is_action_just_pressed("shoot"):
		emit_signal("create_bullet", bullet, global_position)








func _on_Hitbox_area_entered(area):
	if area.is_in_group("Enemy"):
		area.get_parent().queue_free()
		emit_signal("create_bullet", particles, global_position)
		queue_free()
