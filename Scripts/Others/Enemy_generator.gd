extends Position2D

var enemy = preload("res://Characters/Enemy.tscn")

signal create_enemy(enemy, location)

func _on_Timer_timeout():
	randomize()
	emit_signal("create_enemy", enemy, Vector2(global_position.x, rand_range(0, 200)))
